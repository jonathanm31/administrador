<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index');
Route::get('denied',function(){
   return view('auth.permisos');
});
$this->get('logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'guest_group'],function(){
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $this->post('password/reset', 'Auth\PasswordController@reset');
});


Route::group(['middleware' => 'admin_group'],function(){
    // Registration Routes...
    $this->get('register', 'Auth\AuthController@showRegistrationForm');
    $this->post('register', 'Auth\AuthController@register');

    Route::get('admin_panel' , 'HomeController@adminPanel');

});


Route::group(['middleware' => 'usuario_group'],function(){

    Route::get('user_panel' , 'HomeController@userPanel');
});
